import os
import sys

CARACTERE_FIN_DE_LIGNE = '\u2063'
EXTENSION_TABLE = "got"
EXTENSION_BASE_DE_DONNEES = "godb"


class Table:

    nom: str
    base: str
    contenu: str
    dernier_id: int
    clef: str
    valeur: str

    def __init__(self, base: str, nom: str, clef: str, valeur: str):

        # si la table n existe pas
        if not os.path.exists(f"{nom}.{EXTENSION_TABLE}"):
            self.nom = nom
            self.base = base
            self.clef = clef
            self.valeur = valeur
            self.dernier_id = 0
            tete = nom + CARACTERE_FIN_DE_LIGNE + clef + \
                CARACTERE_FIN_DE_LIGNE + valeur
            fichier = open(nom + "." + EXTENSION_TABLE, "w", encoding="utf8")
            fichier.write(tete)
            self.contenu = tete
            fichier.close()

        else:
            print("Chargement de la table !")
            self.charger(nom)

    def charger(self, path: str, base : str):
        """
        Charge une table a l aide d un fichier table
        """
        fichier = open(f"{base}/{nom}.{EXTENSION_TABLE}", 'r', encoding="utf8")
        tete = fichier.read().split(CARACTERE_FIN_DE_LIGNE)[:3]
        self.nom, self.clef, self.valeur = tete
        return self.nom


    def modifier(self, clef, valeur):
        pass

    def est_corrompue(self) -> bool:
        len(self.contenu.split(CARACTERE_FIN_DE_LIGNE)) % 3 != 0

    def donnees_brutes(self) -> list[tuple[int]]:
        if self.est_corrompue() : raise TableCorrompue()
        tab = self.contenu.split(CARACTERE_FIN_DE_LIGNE)
        return [(tab[v], tab[v + 1], tab[v + 2]) for v in range(0, len(tab), 3)]

    def ajouter(self, clef, valeur):
        self.contenu += CARACTERE_FIN_DE_LIGNE + str(self.dernier_id) + CARACTERE_FIN_DE_LIGNE + clef + \
            CARACTERE_FIN_DE_LIGNE + valeur
        self.dernier_id += 1
        fichier = open(f"{self.base}/{self.nom}.{EXTENSION_TABLE}", "w", encoding="utf8")
        fichier.write(self.contenu)
        fichier.close()

    def __getitem__(self, i):
        """
        Renvoie l element en fonction du type de i.
        Si i = int renvoie en fonction de l id
        Sinon renvoie en fonction de la clef
        """
        if isinstance(i, int) :
            for t in self.donnees_brutes():
                if t[0] == i : return t[2]
            raise ElementIntrouvable()
        elif isinstance(i, str):
            for t in self.donnees_brutes():
                if t[1] == i : return t[2]
            raise ElementIntrouvable()
        raise TypeError("L index doit etre un entier ou une chaine de caracteres")


class Base:

    nom: str
    tables: dict[str, Table] = {}

    def __init__(self, nom: str, *args: str):
        fichier_bdd = open(f"../{nom}.{EXTENSION_BASE_DE_DONNEES}", "w", encoding="utf8")
        a_ecrire = f"{nom}"
        self.nom = nom
        os.mkdir(nom)
        os.chdir(nom)
        for table in args:
            a_ecrire += f"\n{table}"
            self.tables[table] = Table(self.nom, table, "defaut", "defaut")
        os.chdir("..")
        fichier_bdd.close()


    def modifier_nom_table(self, table: str, clef: str, valeur: str):
        self.tables[table].modifier_nom(clef, valeur)



class TableCorrompue(Exception):
    pass

class ElementIntrouvable(Exception):
    pass
